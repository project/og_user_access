<?php

/**
 * Access plugin that provides OG permission-based access control.
 */
class og_views_plugin_access_og_perm extends views_plugin_access {
  const default_permission = 'edit group';

  function access($account) {
    return og_user_access_from_context($this->options['perm'], $account);
  }

  function get_access_callback() {
    return array('og_user_access_from_context', array($this->options['perm']));
  }

  function summary_title() {
    return t($this->options['perm']);
  }

  /**
   * Views 3 compatibility.
   *
   * @see views_plugin_access::option_definition()
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['perm'] = array('default' => self::default_permission);

    return $options;
  }

  /**
   * Views 2 compatibility.
   *
   * @deprecated
   *
   * @param array $options
   */
  function option_defaults(&$options) {
    $options['perm'] = self::default_permission;
  }

  function options_form(&$form, &$form_state) {
    $perms = array();
    // Get list of permissions
    foreach (og_get_permissions() as $perm => $info) {
      if (!isset($perms[$info['module']])) {
        $perms[$info['module']] = array();
      }
      $perms[$info['module']][$perm] = strip_tags($info['title']);
    }
    $form['perm'] = array(
      '#type' => 'select',
      '#options' => $perms,
      '#title' => t('Group Permission'),
      '#default_value' => $this->options['perm'],
      '#description' => t('Only users with the selected permission flag will be able to access this display. Note that users with "access all views" can see any view, regardless of other permissions.'),
    );
  }
}
