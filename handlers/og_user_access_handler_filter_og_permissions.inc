<?php

/**
 * Filter a view by group nodes the current user has access to..
 *
 * @ingroup views_filter_handlers
 */
class og_user_access_handler_filter_og_permissions extends views_handler_filter {

  function query() {
    // Fetch groups in query.
    if ($this->options['exposed']) {
      $gids = $this->value;
    }
    else {
      if (!isset($this->value_options)) {
        $this->get_value_options();
      }
      if (is_array($this->value_options)) {
        $gids = array_keys($this->value_options);
      }
    }
    $this->ensure_my_table();
    $field = "$this->table_alias.nid";
    if ($gids) {
      if (count($gids) == 1) {
        $this->query->add_where($this->options['group'], "$field = %d", $gids);
      }
      else {
        $placeholders = db_placeholders($gids);
        $this->query->add_where($this->options['group'], "$field IN ($placeholders)", $gids);
      }
    }
  }

  function get_value_options() { 
    global $user;
    $this->value_options = array();
    foreach ($user->og_groups as $gid => $group) {
      $access = og_user_access($gid, $this->options['og_permission']);
      if ($access) {
        $this->value_options[$gid] = filter_xss($group['title']);
      }
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['og_permission'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $perms = array();
    // Get list of permissions
    foreach (og_get_permissions() as $perm => $info) {
      if (!isset($perms[$info['module']])) {
        $perms[$info['module']] = array();
      }
      $perms[$info['module']][$perm] = strip_tags($info['title']);
    }
    $form['og_permission'] = array(
      '#type' => 'select',
      '#options' => $perms,
      '#title' => t('Group Permission'),
      '#default_value' => $this->options['og_permission'],
      '#description' => t('Only groups the current user has this permission in will be displayed.'),
    );
  }

  function value_form(&$form, &$form_state) {
    if (!isset($this->value_options)) {
      $this->get_value_options();
    }
    if ($this->options['exposed'] && is_array($this->value_options)) {
      $form['value'] = array(
        '#type' => 'select',
        '#title' => 'Groups',
        '#multiple' => TRUE,
        '#options' => $this->value_options,
        '#default_value' => $default_value,
      );
    }
  }

  function accept_exposed_input($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    // If it's optional and there's no value don't bother filtering.
    if ($this->options['expose']['optional'] && empty($this->validated_exposed_input)) {
      return FALSE;
    }

    $rc = parent::accept_exposed_input($input);
    if ($rc) {
      // If we have previously validated input, override.
      if (isset($this->validated_exposed_input)) {
        $this->value = $this->validated_exposed_input;
      }
    }

    return $rc;
  }

  function exposed_validate(&$form, &$form_state) {
    if (empty($this->options['exposed'])) {
      return;
    }

    $identifier = $this->options['expose']['identifier'];

    if (empty($this->options['expose']['identifier'])) {
      return;
    }

    $this->validated_exposed_input = $form_state['values'][$this->options['expose']['identifier']];
  }


  function value_submit($form, &$form_state) {
    // prevent array_filter from messing up our arrays in parent submit.
  }

  function expose_form_right(&$form, &$form_state) {
    parent::expose_form_right($form, $form_state);
    if ($this->options['type'] != 'select') {
      unset($form['expose']['reduce']);
    }
  }

  function admin_summary() {
    return ($this->options['exposed'])?'exposed':'';
  }
}
