<?php
// $Id$

/**
 * @file
 * Reimplement Drupal 7 API functions as needed.
 */

if (!function_exists('drupal_render_children')) {
  /**
   * Render children of an element and concatenate them.
   *
   * This renders all children of an element using drupal_render() and then
   * joins them together into a single string.
   *
   * @param $element
   *   The structured array whose children shall be rendered.
   * @param $children_keys
   *   If the keys of the element's children are already known, they can be passed
   *   in to save another run of element_children().
   */
  function drupal_render_children(&$element, $children_keys = NULL) {
    if ($children_keys === NULL) {
      $children_keys = element_children($element);
    }
    $output = '';
    foreach ($children_keys as $key) {
      if (!empty($element[$key])) {
        $output .= drupal_render($element[$key]);
      }
    }
    return $output;
  }
}
