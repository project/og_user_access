<?php

/**
 * @file
 * Defile handlers and data for views integration.
 */

/**
 * Implementation of hook_views_handlers().
 */
function og_user_access_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'og_user_access') .'/handlers',
    ),
    'handlers' => array(
      // filter
      'og_user_access_handler_filter_og_permissions' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugins().
 */
function og_user_access_views_plugins() {
  return array(
    'module' => 'og_user_access',
    'access' => array(
      'og_permission' => array(
        'title' => t('OG Permission'),
        'help' => t('User has a permission in the current Organic Group.'),
        'handler' => 'og_views_plugin_access_og_perm',
        'uses options' => TRUE,
        'path' => drupal_get_path('module', 'og_user_access') .'/plugins',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function og_user_access_views_data_alter(&$data) {
  $data['og_uid']['has_permission'] = array(
    'real field' => 'nid',
    'title' => t('OG: Member has permission in group'),
    'help' => t('<strong>Groups</strong> are filtered by those the current user has a certain permission in.'),
    'filter' => array(
      'handler' => 'og_user_access_handler_filter_og_permissions',
    ),
  );
}
