<?php
function og_user_access_override_field($node_type) {
  $field = array (
    'field_name' => OG_DEFAULT_ACCESS_FIELD,
    'type_name' => $node_type,
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '0',
    'multiple' => '0',
    'db_storage' => '1',
    'module' => 'number',
    'active' => '1',
    'locked' => '0',
    'columns' =>
    array (
      'value' =>
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => true,
      ),
    ),
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '0|Use default roles and permissions
    1|Override default roles and permissions',
    'allowed_values_php' => '',
    'widget' =>
    array (
      'default_value' =>
      array (
        0 =>
        array (
          'value' => NULL,2
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Roles and Permissions',
      'weight' => '31',
      'description' => '',
      'type' => 'optionwidgets_onoff',
      'module' => 'optionwidgets',
    ),
  );
}
